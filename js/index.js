$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
    $('.carousel').carousel({
      interval: 2000
    });
    $('#contactoMatriculas').on('show.bs.modal', function (e) {
      console.log('Mostrando el modal llamado Contacto Matriculas');
      $('#contactoBtn').removeClass('btn-outline-success');
      $('#contactoBtn').addClass('btn-primary');
      $('#contactoBtn').prop('disabled', true);
    });
    $('#contactoMatriculas').on('shown.bs.modal', function (e) {
      console.log('Mostró el modal llamado Contacto Matriculas');
    });
    $('#contactoMatriculas').on('hide.bs.modal', function (e) {
      console.log('Ocultando el modal llamado Contacto Matriculas');
      $('#contactoBtn').prop('disabled', false);
      $('#contactoBtn').addClass('btn-outline-success');
    });
    $('#contactoMatriculas').on('hide.bs.modal', function (e) {
      console.log('Ocultó el modal llamado Contacto Matriculas');
    });
   });